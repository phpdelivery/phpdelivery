-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-06-2009 a las 23:55:07
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `farmaciarex`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariosAdmin`
--

CREATE TABLE `usuariosAdmin` (
  `id` smallint(3) unsigned zerofill NOT NULL auto_increment,
  `usuario` varchar(100) character set utf8 collate utf8_spanish_ci NOT NULL,
  `contrasenia` varchar(50) character set utf8 collate utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) character set utf8 collate utf8_spanish_ci NOT NULL,
  `apellido` varchar(150) character set utf8 collate utf8_spanish_ci NOT NULL,
  `fechaNac` date default NULL,
  `direccion` varchar(200) character set utf8 collate utf8_spanish_ci NOT NULL,
  `ciudad` enum('01','02','03','04','05','06','07','08','09') default NULL,
  `cp` mediumint(4) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `usuariosAdmin`
--

INSERT INTO `usuariosAdmin` (`id`, `usuario`, `contrasenia`, `nombre`, `apellido`, `fechaNac`, `direccion`, `ciudad`, `cp`, `telefono`) VALUES
(001, 'Mel', '1234', 'Melisa', 'Alvarez Terán', '0000-00-00', 'Diaz Velez 495', '02', 1832, '42450598'),
(002, 'grgsgh', 'dfhrt', 'jhfgjfj', 'hdfhdf', '0000-00-00', 'jfgjfgjkdfb tjdf', '07', 3456, 'hdfjhtyjf');
