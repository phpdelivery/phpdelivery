<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
	<head>
		<title>Farmacia Rex</title>
		<style type="text/css" media="all">
			@import "css/estilo_admin.css";
		</style>
	</head>
	<body>
		<?php
        // archivo de configuracion
        require('config.inc.php');
        
        session_start();
        
        if($_SESSION["adminlogueado"]){
            echo '<a href="controles/cerrarsesion.php" class="cerrar">Cerrar Sesi�n</a>';
        }else{
            header('location:pantallalogueo.php');
        }
                                
        
        echo '
		  
		  <p class="title">Seleccione la categoria que desea editar:</p>
		  
          <form name="menu" action="'.$PHP_SELF.'" method="post" class="opciones">
          
              <input type="submit" name="ar" value="Alta Usuario">
              <input type="submit" name="br" value="Baja Usuario">
			  <input type="submit" name="vr" value="Ver Usuarios">

          </form>
		  
          <div>';
            if(isset($_POST[ar]) or isset($_POST[br]) or isset($_POST[mr])){
              
              // Alta de registro
              if(isset($_POST[ar]) and isset($_POST[guardar])){ 
				// Guarda registro
				include('conexion.inc.php');
				$sql = "INSERT INTO registros VALUES('','$_POST[titulo]','$_POST[textoIntroduccion]','$_POST[textoCompleto]')";
				$consulta = mysql_query($sql) or die(mysql_error());
				echo 'El registro a sido guardado exitosamente';
				include('cconexion.inc.php');
              }
              else if(isset($_POST[ar]) and !isset($_POST[guardar])){ 
			  
				  // Formulario de carga
				  echo '
				  <form name="Alta" action="'.$PHP_SELF.'" method="post">
					<input type="hidden" name="ar" value="'.$_POST[ar].'">
					<table cellspacing="2" cellpadding="0">
					  <tr>
						<td>Titulo : </td><td><input type="text" name="titulo" size="45"></td>
					  </tr>
					  <tr>
						<td>Introduccion :</td><td><input type="text" name="textoIntroduccion" size="45"></td>
					  </tr>
					  <tr>
						<td>Texto completo : </td><td><textarea name="textoCompleto" cols="45" rows="10"></textarea></td>
					  </tr>
					  <tr>
						<td></td><td><input type="submit" name="guardar" value="Guardar registro"></td>
					  </tr>
					</table>
				  </form>
				  ';
              }
              // Baja de registro
              else if(isset($_POST[br])){
                include('conexion.inc.php');
                 $sql = "DELETE FROM registros WHERE registro_id = '$_POST[registro_id]'";
                 $consulta = mysql_query($sql) or die(mysql_error());
                 echo 'El registro a sido eliminado exitosamente';
                include('cconexion.inc.php');
              }

		  // Ver registros
              else if(isset($_POST[vr])){
                            echo '
              <table cellspacing="0" cellpadding="5">
                <tr>
                  <th>Registro ID</th><th>Titulo</th><th>Introduccion</th><th colspan="3">Tareas de administracion</th>
                </tr>
            ';
              $color = 1;
              include('controles/conexion.php');
               $sql = "SELECT * FROM usuarios";
               $consulta = mysql_query($sql) or die(mysql_error());
               while($resultados = mysql_fetch_array($consulta)){
                 echo '
                  <form name="principal" action="'.$PHP_SELF.'" method="post">
                  <input type="hidden" name="registro_id" value="'.$resultados[registro_id].'">
                  <tr bgcolor="';
                  if($color == 1){
                    echo '#BFBFBF';
                    $color = 2;
                  } else {
                    echo '#D0D0D0';
                    $color = 1;
                  }
                  echo'">
                    <td>'.$resultados[registro_id].'</td><td>'.$resultados[registro_titulo].'</td><td>'.$resultados[registro_introduccion].'</td><td>      <input type="submit" name="mr" value="Modifica registro"></td><td><input type="submit" name="br" value="Baja registro"></td>
                  </tr>
                  </form>
                 ';
               }
              include('cconexion.inc.php');
            echo '
              </table>
            ';

              }

              // Modifica registro
              else if(isset($_POST[mr]) and isset($_POST[guardar]) and isset($_POST[registro_id])){ // Guarda modificaciones al registro
                include('conexion.inc.php');
                 $sql = "UPDATE registros SET registro_titulo='$_POST[titulo]', registro_introduccion='$_POST[textoIntroduccion]', registro_completo='$_POST[textoCompleto]' WHERE registro_id = '$_POST[registro_id]'";
                 $consulta = mysql_query($sql) or die(mysql_error());
                include('cconexion.inc.php');
                 echo 'El registro a sido modificado exitosamente';
              }
              else if(isset($_POST[mr]) and isset($_POST[registro_id]) and !isset($_POST[guardar])){ // Formulario de carga
                include('conexion.inc.php');
                 $sql = "SELECT * FROM registros WHERE registro_id ='$_POST[registro_id]'";
                 $consulta = mysql_query($sql) or die(mysql_error());
                 $resultados = mysql_fetch_array($consulta);
                include('cconexion.inc.php');
              echo '
              <form name="Modificacion" action="'.$PHP_SELF.'" method="post">
                <input type="hidden" name="mr" value="'.$_POST[mr].'">
                <input type="hidden" name="registro_id" value="'.$_POST[registro_id].'">
                <table cellspacing="2" cellpadding="0">
                  <tr>
                    <td>Titulo : </td><td><input type="text" name="titulo" size="45" value="'.$resultados[registro_titulo].'"></td>
                  </tr>
                  <tr>
                    <td>Introduccion :</td><td><input type="text" name="textoIntroduccion" size="45" value="'.$resultados[registro_introduccion].'"></td>
                  </tr>
                  <tr>
                    <td>Texto completo : </td><td><textarea name="textoCompleto" cols="45" rows="10">'.$resultados[registro_completo].'</textarea></td>
                  </tr>
                  <tr>
                    <td></td><td><input type="submit" name="guardar" value="Guardar registro"></td>
                  </tr>
                </table>
              </form>
              ';
              }
            } else { // pantalla que veremos por defecto
            }
            echo '</div>';
        ?>
	</body>
</html>