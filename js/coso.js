$(function() { 
	$('.necesario').hide();
	
	$(".button").click(function() {  
	  
		var usuario = $("input#usuario").val(); 
		
		if (usuario == "") {  
			$("label#usuario_error").show();  
			$("input#usuario").focus();  
			return false;  
		}  
		
		var contrasenia = $("input#contrasenia").val();
		var contrasenia2 = $("input#contrasenia2").val();
		
		if (contrasenia == "") {  
			$("label#contra_error").show();  
			$("input#contrasenia").focus();  
			return false;  
		}  
		
		if(contrasenia != contrasenia2){
			$("label#contra2_error").show();
			$("input#contrasenia").focus();  
			return false;  
		}
		
		var mail = $("input#mail").val();
		
		if (mail == "") {  
			$("label#mail_error").show();  
			$("input#mail").focus();  
			return false;  
		}  
		
		var nombre = $("input#nombre").val();
		
		if (nombre == "") {  
			$("label#nombre_error").show();  
			$("input#nombre").focus();  
			return false;  
		}  
		
		var apellidos = $("input#apellidos").val();
		
		if (apellidos == "") {  
			$("label#apellido_error").show();  
			$("input#apellidos").focus();  
			return false;  
		}  
		
		var dia = $("select#dia").val();
		var mes = $("select#mes").val();
		var anio = $("select#anio").val();
		var fechaNac = anio+"-"+mes+"-"+dia;
		
		if (dia == "dia" || mes == "mes" || anio == "anio") {  
			$("label#fecha_error").show();  
			$("select#dia").focus();  
			return false;  
		}  
		
		var direc = $("input#direc").val();
		
		if (direc == "") {  
			$("label#direc_error").show();  
			$("input#direc").focus();  
			return false;  
		}  
		
		var ciudad = $("select#ciudad").val();
		
		if (ciudad == "ciudad") {  
			$("label#ciudad_error").show();  
			$("select#ciudad").focus();  
			return false;  
		}  
		
		var cp = $("input#cp").val();
		
		if (cp == "") {  
			$("label#cp_error").show();  
			$("input#cp").focus();  
			return false;  
		}  
		
		var tel = $("input#tel").val();
		
		if (tel == "") {  
			$("label#tel_error").show();  
			$("input#tel").focus();  
			return false;  
		}  
		
		var dataString = 'usuario='+ usuario + '&contrasenia=' + contrasenia + '&mail=' + mail + '&nombre=' + nombre + '&apellidos=' + apellidos + '&fechaNac=' + fechaNac + '&direc=' + direc + '&ciudad=' + ciudad + '&cp=' + cp + '&tel=' + tel;
		//alert (dataString);
		
		$.ajax({  
			type: "POST",  
			url: "controles/registrarse.php",  
			data: dataString,  
			success: function() {  
				$('.box').css("width","350px");
				$('.box').css("height","200px");
				
				$('.box').html("<div id='message'></div>");
				
				$('#message').html('<span class="formulario_tit">Bienvenido! Ya es usuario de Farmacia Rex!</span><br /><br />')
				
				.append('<p class="p2">Su nombre de usuario es: <span class="usuario">'+usuario+'</span><br /> Sus datos ser�n enviados al e-mail: <span class="usuario">'+mail+'</span></p><a href="compra_paso1.php" class="enviar">Iniciar Sesi�n</a><br /><br /><br /><p class="p2 detalles">No olvide estos datos. Recuerde que con ellos podra acceder nuevamente.</p>')
				
				$(".p2").css("padding-left","20px");
				$(".p2").css("font-weight","bold")
				
				.hide()
				.fadeIn(1500, function() {  
					$('#message'); 
				});  
			}  
		});  
		return false;
	});
	
});

