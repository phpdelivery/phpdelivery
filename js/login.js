$(function() { 
	$('.necesario').hide();
	
	$(".button").click(function() { 
		var user = $("input#nombre").val();
		
		if (user == "") {  
			$("label#usuario_error").show();  
			$("input#nombre").focus();  
			return false;  
		}  
		
		var pass = $("input#pass").val();
		
		if (pass == "") {  
			$("label#contrasenia_error").show();  
			$("input#pass").focus();  
			return false;  
		}  
	});
});