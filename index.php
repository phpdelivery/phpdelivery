<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sistema de Delivery - Pizza Mandy</title>
            <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
            <script type="text/javascript" src="js/login.js"></script>
        	<style type="text/css" media="all">
                @import "css/estilo.css";
            </style>
    </head>

    <body>
    	<div class="cuadro">
        	<div class="login">
                <p class="iniciar">Acceso a Sistema de Delivery</p>
                <form method="post" action="controles/login_admin.php">
                    <div class="caja">
                        <label class="titulos">Usuario: </label>
                        <input id="usuario" name="nombre" type="text" />
                        <label class="necesario" id="usuario_error">Usuario Inválido</label>
                    </div><br />
                    <div class="caja">
                        <label class="titulos">Contraseña: </label>
                        <input id="contra" name="pass" type="password" />
                        <label class="necesario" id="contrasenia_error">Contraseña Inválida</label>
                    </div>
                    <div class="enviar">
                        <input name="Enviar" type="submit" value="Ingresar" />
                    </div>
                </form>
                <br />
                <a href="#">Olvid&eacute; mi contraseña</a>
            </div>
        </div>
    </body>
</html>
